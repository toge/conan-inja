from conans import ConanFile, CMake, tools

class InjaConan(ConanFile):
    name           = "inja"
    version        = "3.0.0"
    license        = "MIT"
    homepage       = "https://github.com/pantor/inja"
    url            = "https://bitbucket.org/toge/conan-inja/"
    description    = "A Template Engine for Modern C++"
    no_copy_source = True
    requires       = "jsonformoderncpp/[>= 3.7.0]"

    def source(self):
        tools.download("https://github.com/pantor/inja/releases/download/v{}/inja.hpp".format(self.version), "inja.hpp")

    def package(self):
        self.copy("inja.hpp", src=".", dst="include/inja", keep_path=False)

    def package_info(self):
        self.info.header_only()

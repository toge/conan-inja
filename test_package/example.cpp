#include <cstdlib>
#include <iostream>

#include "inja/inja.hpp"
#include "nlohmann/json.hpp"

int main(int argc, const char** argv) {
    auto json = nlohmann::json {};
    json["name"] = "world";

    auto result = inja::render("Hello {{ name }}!", json);

    assert(result == "Hello world!");

    return 0;
}
